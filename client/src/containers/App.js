import '../styles/App.sass'
import { connect } from "react-redux"

import Settings from './views/Settings'
import Game from './views/Game'
import Build from './views/Build'

function App(props) {

  function view() {
    switch (props.view) {
      case "settings": return (
        <Settings></Settings>
      )
      case "build": return (
        <Build></Build>
      )
      case "game": return (
        <Game></Game>
      )
      default: return (
        <h1>no such view</h1>
      )
    }
  }

  return (
    <>
      {view()}
    </>
  );
}

const mapStateToProps = (state) => ({
  view: state.view.view
});
export default connect(mapStateToProps)(App)

