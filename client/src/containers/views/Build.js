import React, { useEffect, useRef, useState } from 'react';
import Loading from "../components/Loading";
import { connect } from "react-redux"

const mockup = (!process.env.NODE_ENV || process.env.NODE_ENV === 'development')
const characters = 'abcdefghijklmnopqrstuvwxyz'

function Build(props) {

    const self = useRef(null)
    const [loaded, setLoaded] = useState(false)

    useEffect(() => {
        if (!loaded && self) {
            setTimeout(() => {
                self.current.style.opacity = '1'
            }, 1)
            setLoaded(true)
            countHealth()
            buildTree()
        }
        // eslint-disable-next-line
    }, [loaded])

    const countHealth = () => {
        let health = props.deepnes
        props.dispatch({type: 'SetMaxHealth', value: health})
        props.dispatch({type: 'SetHealth', value: health})
    }

    const buildTree = async () => {

        if (!props.direction && mockup && props.root.length < props.deepnes + 2) {
            alert(`Too short root stimulus (${props.deepnes + 2} chars minimum)`)
            window.location.reload()
        }

        var tree = {}
        
        var answer = generateAnswer()
        var select = generateSelect(answer)

        if (props.direction) {
            tree.tree = [{word: props.root, tree: await buildTreeResponse(props.root, props.deepnes)}]
            await fillTreeResponse(tree.tree, 0, answer)
        } else {
            props.dispatch({type: 'SetAnswerResult', value: props.root})
            var stimulusChain = [props.root]
            for (var i = 0; i < props.deepnes + 1; i++) {
                stimulusChain.unshift(mockup ? (await getStimulusMock(stimulusChain[0])) : (await getStimulus(stimulusChain[0]))[0])
            }
            tree.tree = [{word: stimulusChain[0], tree: await buildTreeStimulus(stimulusChain, props.deepnes, [0], answer, stimulusChain[0])}]
        }

        props.dispatch({type: 'SetAnswer', value: answer})
        props.dispatch({type: 'SetSelect', value: select})
        props.dispatch({type: 'SetTree', value: tree})
        if (mockup) {
            setTimeout(() => {props.dispatch({type: 'SetView', value: 'game'})}, 1000)
        } else {
            props.dispatch({type: 'SetView', value: 'game'})
        }
    }

    const generateAnswer = () => {
        var answer = [0]
        for (var i = 0; i <= props.deepnes; i++) {
            answer.push(Math.random() > .5 ? 1 : 0)
        }
        return answer
    }

    const generateSelect = answer => {
        var select = [...answer]
        for (var i = 1; i < select.length - 1; i++) {
            select[i] = -1
        }
        return select
    }

    const buildTreeResponse = async (word, layers) => {
        if (layers === 0) return [{word: ''}, {word: ''}]
        var responses = mockup ? (await getResponseMock(word)) : (await getResponse(word))
        return [
            {
                word: responses[0], 
                tree: await buildTreeResponse(responses[0], layers - 1)
            }, 
            {
                word: responses[1], 
                tree: await buildTreeResponse(responses[1], layers - 1)
            }
        ]
    }

    const fillTreeResponse = async (tree, i, answer, pWord = '') => {
        if (i !== answer.length - 1) {
            await fillTreeResponse(tree[answer[i]].tree, i + 1, answer, tree[answer[i]].word)
        } else {
            let word = (mockup ? (await getResponseMock(pWord)) : (await getResponse(pWord)))[answer[i]]
            tree[answer[i]].word = word
            props.dispatch({type: 'SetAnswerResult', value: word})
        }
    }

    const buildTreeStimulus = async (stimulusChain, layers, currentPattern, answer, pWord = '') => {
        var result = [{word: ''}, {word: ''}]
        if (checkPartOfPath(currentPattern, answer)) {
            result[answer[currentPattern.length]].word = stimulusChain[currentPattern.length]
        }
        if (currentPattern.length !== answer.length) {
            var responses = mockup ? (await getResponseMock(pWord)) :(await getResponse(pWord)) 
            if (result[0].word === '') result[0].word = responses[0]
            if (result[1].word === '') result[1].word = responses[1]
        }
        if (layers === 0) return result
        return [
            {
                word: result[0].word, 
                tree: await buildTreeStimulus(stimulusChain, layers - 1, [...currentPattern, 0], answer, result[0].word)
            }, 
            {
                word: result[1].word, 
                tree: await buildTreeStimulus(stimulusChain, layers - 1, [...currentPattern, 1], answer, result[1].word)
            }
        ]
    }

    const checkPartOfPath = (path, answer) => {
        for (var i = 0; i < path.length; i++) {
            if (path[i] !== answer[i]) return false
        }
        return true
    }

    const getResponseMock = async word => {
        return [word + getRandomChar(), word + getRandomChar()]
    }

    const getStimulusMock = async word => {
        return word.slice(0, -1)
    }

    const getRandomChar = () => characters.charAt(Math.floor(Math.random() * characters.length))

    const getResponse = async word => {
        var response = await fetch(`/r?word=${word}`).then(r => r.json())
        if (response[0] === "error") {
            alert('Не удалось построить дерево ассоциаций.');
            window.location.reload()
        }
        return response
    }

    const getStimulus = async word => {
        var response = await fetch(`/s?word=${word}`).then(r => r.json())
        if (response[0] === "error") {
            alert('Не удалось построить дерево ассоциаций.');
            window.location.reload()
        }
        return response
    }

    return (
        <div className='build' ref={self}>
            <Loading></Loading>
        </div>
    )
}

const mapStateToProps = (state) => ({
    direction: state.settings.direction,
    root: state.settings.root,
    deepnes: state.settings.deepnes,
    answer: state.game.answer
});
export default connect(mapStateToProps)(Build)