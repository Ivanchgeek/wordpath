import React, { useEffect, useRef, useState } from 'react';
import { connect } from "react-redux"
import Go from '../components/Go'

function Settings(props) {

    const range = useRef(null)
    const value = useRef(null)
    const rootWrap = useRef(null)
    const rootInput = useRef(null)
    const [loaded, setLoaded] = useState(false)
    const [hideButton, setHideButton] = useState(false)
    const [buttonOnClickName, setButtonOnClickName] = useState('deepnes')

    useEffect(() => {
        if (!loaded) {
            range.current.value = props.deepnes * 1000
            setLoaded(true)
        }
    }, [loaded, props.deepnes])

    const change = event => {
        let val = Math.round(event.target.value / 1000)
        if (val !== props.deepnes) props.dispatch({ type: 'SetDeepnes', value: val })
    }

    const saveDeepnes = () => {
        range.current.style.opacity = '0'
        range.current.style.transform = 'translate(-50%, -10vh)'
        value.current.style.opacity = '0'
        value.current.style.transform = 'translate(-50%, -10vh)'
        rootWrap.current.style.display = 'flex'
        setTimeout(() => {
            rootWrap.current.style.opacity = '1'
        }, 300)
        setTimeout(() => {
            range.current.style.display = 'none'
            value.current.style.display = 'none'
        }, 200)
        setButtonOnClickName('root')
    }

    const saveRoot = () => {
        if (props.root === '' || props.root === undefined || props.root.indexOf(' ') !== -1) {
            rootInput.current.classList.add('error')
            rootInput.current.animate([
                {transform: 'translateX(0%)'},
                {transform: 'translateX(-2%)'},
                {transform: 'translateX(0%)'},
                {transform: 'translateX(2%)'},
                {transform: 'translateX(0%)'},
            ], {
                duration: 200
            })
            return
        }
        setHideButton(true)
        setTimeout(() => {
            rootWrap.current.style.transform = 'translate(-50%, -20vh)'
            rootWrap.current.style.opacity = '0'
            setTimeout(() => {
                setTimeout(() => { props.dispatch({ type: 'SetView', value: 'build'}) }, 200)
            }, 200)
        }, 100)
    }

    const buttonOnClick = () => {
        switch (buttonOnClickName) {
            case 'deepnes': 
                saveDeepnes()
                break
            case 'root': 
                saveRoot()
                break
            default: alert('onclick error')
        }
    }

    const rootUpdate = event => {
        props.dispatch({type: 'SetRoot', value: event.target.value.toLowerCase()})
    }

    return (
        <div className='deepnes'>

            <input type='range' min='2000' max='4000' onChange={change} ref={range}></input>
            <h1 className='value' ref={value}>Глубина ассоциаций: {props.deepnes}</h1>

            <div className='rootWrap' ref={rootWrap}>
                <div className={`switchWrap ${props.direction ? 'start' : 'finish'}`}>
                    <h2 onClick={() => {props.dispatch({type: 'SetDirection', value: true})}}>Начать со слова </h2>
                    <h2 onClick={() => {props.dispatch({type: 'SetDirection', value: false})}}>Закончить словом </h2>
                </div>
                <input type='text' onChange={rootUpdate} ref={rootInput}></input>
            </div>

            <Go onClick={buttonOnClick} hide={hideButton}></Go>
        </div>
    );
}

const mapStateToProps = (state) => ({
    deepnes: state.settings.deepnes,
    direction: state.settings.direction,
    root: state.settings.root
});
export default connect(mapStateToProps)(Settings)
