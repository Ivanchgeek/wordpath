import React, { useEffect, useRef, useState } from 'react';
import { connect } from "react-redux"
import CheckAnswer from '../components/CheckAnswer';
import ResultBar from '../components/ResultBar'
import ResultLine from '../components/ReultLine';
import ToolBar from '../components/ToolBar';

const answerAnimationDelay = 600

function Game(props) {

  const self = useRef(null)
  const [loaded, setLoaded] = useState(false)
  const [tilesContent, setTilesContent] = useState([])
  const [gameStaus, setGameStatus] = useState(-1)

  useEffect(() => {
    if (!loaded && self) {
      setTimeout(() => {
        self.current.style.opacity = '1'
      }, 1)
      setLoaded(true)
    }
  }, [loaded])

  useEffect(() => {
    var tilesContentTemp = []
    var tree = props.tree
    for (let i = 0; i < props.select.length; i++) {
      if (i === props.select.length - 1) {
        tilesContentTemp.push([props.answerResult, ''])
        continue
      }
      if (props.select[i] === -1 && i !== 0 && props.select[i - 1] !== -1) {
        tilesContentTemp.push([tree.tree[0].word, tree.tree[1].word])
        tree = tree.tree[props.answer[i]]
        continue
      }
      if (props.select[i] === -1) {
        tilesContentTemp.push(['', ''])
        tree = tree.tree[props.answer[i]]
        continue
      }
      if (props.select[i] !== -1) {
        tree = tree.tree[props.select[i]]
        if (props.select[i] === 0) {
          tilesContentTemp.push([tree.word, ''])
        } else {
          tilesContentTemp.push(['', tree.word])
        }
        continue
      }
    }
    setTilesContent(tilesContentTemp)

    if (props.select.indexOf(-1) === -1) {
      proccessResult()
    }
    // eslint-disable-next-line
  }, [props.select, props.answer, props.tree, props.answerResult])

  const branchContent = (i) => {
    if (tilesContent[i]) {
      let hoverable = true
      var onClick_1 = () => {}
      var onClick_2 = () => {}
      var style_1 = {opacity: '.5', top: `${100 / 3}%`}
      var style_2 = {opacity: '.5', top: `${100 / 3 * 2}%`}
      if (tilesContent[i][0] === '' && tilesContent[i][1] === '') {
        style_1.opacity = '0'
        style_2.opacity = '0'
        hoverable = false
      }
      if (tilesContent[i][0] !== '' && tilesContent[i][1] === '') {
        style_1.top = '50%'
        style_1.opacity = '1'
        style_2.opacity = '0'
        hoverable = false
      }
      if (tilesContent[i][0] === '' && tilesContent[i][1] !== '') {
        style_2.top = '50%'
        style_2.opacity = '1'
        style_1.opacity = '0'
        hoverable = false
      }
      hoverable = hoverable && gameStaus === -1
      if (hoverable) {
        onClick_1 = () => {
          let selectTemp = props.select
          selectTemp[i] = 0
          props.dispatch({type: 'SetSelect', value: selectTemp})
        }
        onClick_2 = () => {
          let selectTemp = props.select
          selectTemp[i] = 1
          props.dispatch({type: 'SetSelect', value: selectTemp})
        }
      }
      return (
        <>
          <div className={`word ${hoverable ? 'hoverable' : ''}`} style={style_1} onClick={onClick_1}>{tilesContent[i][0]}</div>
          <div className={`word ${hoverable ? 'hoverable' : ''}`} style={style_2} onClick={onClick_2}>{tilesContent[i][1]}</div>
        </>
      )
    }
  }

  const proccessResult = () => {
    if (gameStaus === -1) {
      let win = true
      props.select.forEach((s, i) => {
        if (s !== props.answer[i]) win = false
      })
      setGameStatus(win ? 1 : 0)
    }
  }

  const checkAnswer = () => {
    setGameStatus(2)
    let correctEnd = 0
    for (var i = 1; i < props.select.length - 2; i++) {
      if (props.select[i] !== props.answer[i]) break
      correctEnd = i
    }
    eraseSelect(props.select.length - 2, correctEnd)
    setTimeout(() => {
      buildSelectToAnswer(correctEnd + 1)
      setTimeout(() => {
        setGameStatus(3)
      }, answerAnimationDelay * (props.select.length - 2 - correctEnd))
    }, answerAnimationDelay * (props.select.length - 2 - correctEnd))
  }

  const eraseSelect = (i, end) => {
    let selectTemp = props.select
    selectTemp[i] = -1
    props.dispatch({type: 'SetSelect', value: selectTemp})
    if (i > end + 1) setTimeout(() => {eraseSelect(i - 1, end)}, answerAnimationDelay)
  }

  const buildSelectToAnswer = i => {
    let selectTemp = props.select
    selectTemp[i] = props.answer[i]
    props.dispatch({type: 'SetSelect', value: selectTemp})
    if (i < props.select.length - 2) setTimeout(() => {buildSelectToAnswer(i + 1)}, answerAnimationDelay)
  }

  return (
    <div className="game" ref={self}>

        {props.select.map((_, i) => {
          return (
            <div className='branchWrap' style={{flex: props.select.length - 1}} key={i}>
              {branchContent(i)}
            </div>
          )
        })}

        <ToolBar hidden={gameStaus !== -1}></ToolBar>
        <ResultBar hidden={gameStaus === -1} result={gameStaus === 1}></ResultBar>
        <ResultLine active={gameStaus === 1 || gameStaus === 3}></ResultLine>
        <CheckAnswer active={gameStaus === 0} onClick={checkAnswer}></CheckAnswer>

    </div>
  );
}

const mapStateToProps = (state) => ({
  deepnes: state.settings.deepnes,
  select: state.game.select,
  tree: state.game.tree,
  answer: state.game.answer,
  maxHealth: state.game.maxHealth,
  health: state.game.health,
  answerResult: state.game.answerResult
});
export default connect(mapStateToProps)(Game)
