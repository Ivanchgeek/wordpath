
function ResultBar(props) {
    return (
        <div className={`resultBar ${props.hidden ? 'hidden' : ''}`}>
            <h2>{props.result ? 'Вы победили 🥳' : 'Вы проиграли 😔'}</h2>
            <p onClick={() => {window.location.reload()}}>новая игра</p>
        </div>
    )
}

export default ResultBar