function ResultLine(props) {
    return (
        <div className={`resultLine ${props.active ? 'active' : ''}`}></div>
    )
}

export default ResultLine