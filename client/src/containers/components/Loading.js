function Loading() {

    const speed = '.25s'

    return (
        <svg width="200" height="236" viewBox="0 0 200 236" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="102" cy="17" r="17" fill="white" />
            <circle cx="102" cy="219" r="17" fill="white" />
            <circle cx="183" cy="112" r="17" fill="white" />
            <circle cx="17" cy="112" r="17" fill="white" />
            <circle cx="102" cy="112" r="17" fill="white" />
            <line x1="101.506" y1="15.6841" x2="101.506" y2="15.6841" stroke="white" strokeWidth="4">
                <animate attributeName='x2' from='101.50' to='184.506' dur={speed} fill='freeze' begin='0s;4.begin;' id='1'/>
                <animate attributeName='y2' from='15.6841' to='110.684' dur={speed} fill='freeze' begin='1.begin;'/>
                <animate attributeName='x1' from='101.506' to='184.506' dur={speed} fill='freeze' begin='2.begin'/>
                <animate attributeName='y1' from='15.6841' to='110.684' dur={speed} fill='freeze' begin='2.begin'/>

                <animate attributeName='x2' from='184.506' to='101.506' dur='.01s' fill='freeze' begin='2.end;'/>
                <animate attributeName='y2' from='110.684' to='15.6841' dur='.01s' fill='freeze' begin='2.end;'/>
                <animate attributeName='x1' from='184.506' to='101.506' dur='.01s' fill='freeze' begin='2.end;'/>
                <animate attributeName='y1' from='110.684' to='15.6841' dur='.01s' fill='freeze' begin='2.end;'/>
            </line>
            <line x1="101.506" y1="18.3159" x2="101.506" y2="18.3159" stroke="white" strokeWidth="4">
                <animate attributeName='x2' from='101.506' to='18.5061' dur={speed} fill='freeze' begin='1.begin'/>
                <animate attributeName='y2' from='18.3159' to='113.316' dur={speed} fill='freeze' begin='1.begin'/>
                <animate attributeName='x1' from='101.506' to='18.5061' dur={speed} fill='freeze' begin='2.begin'/>
                <animate attributeName='y1' from='18.3159' to='113.316' dur={speed} fill='freeze' begin='2.begin'/>

                <animate attributeName='x2' from='101.506' to='101.506' dur='.01s' fill='freeze' begin='2.end;'/>
                <animate attributeName='y2' from='113.316' to='18.3159' dur='.01s' fill='freeze' begin='2.end;'/>
                <animate attributeName='x1' from='101.506' to='101.506' dur='.01s' fill='freeze' begin='2.end;'/>
                <animate attributeName='y1' from='113.316' to='18.3159' dur='.01s' fill='freeze' begin='2.end;'/>
            </line>
            <line x1="102" y1="219" x2="102" y2="219" stroke="white" strokeWidth="4">
                <animate attributeName='y1' from='219' to='17' dur={speed} fill='freeze' begin='2.end' id='3'/>
                <animate attributeName='y2' from='219' to='17' dur={speed} fill='freeze' begin='3.end' id='4'/>

                <animate attributeName='y1' from='17' to='219' dur={speed} fill='freeze' begin='4.end'/>
                <animate attributeName='y2' from='17' to='219' dur={speed} fill='freeze' begin='4.end'/>
            </line>
            <line x1="184.58" y1="113.226" x2="184.58" y2="113.226" stroke="white" strokeWidth="4">
                <animate attributeName='x2' from='184.58' to='101.58' dur={speed} fill='freeze' begin='1.end' id='2'/>
                <animate attributeName='y2' from='113.22' to='220.226' dur={speed} fill='freeze' begin='2.begin'/>
                <animate attributeName='x1' from='184.58' to='101.58' dur={speed} fill='freeze' begin='3.begin'/>
                <animate attributeName='y1' from='113.22' to='220.226' dur={speed} fill='freeze' begin='3.begin'/>

                <animate attributeName='x2' from='101.58' to='184.58' dur='.01s' fill='freeze' begin='3.end;'/>
                <animate attributeName='y2' from='113.22' to='113.226' dur='.01s' fill='freeze' begin='3.end;'/>
                <animate attributeName='x1' from='101.58' to='184.58' dur='.01s' fill='freeze' begin='3.end;'/>
                <animate attributeName='y1' from='113.22' to='113.226' dur='.01s' fill='freeze' begin='3.end;'/>
            </line>
            <line x1="18.5803" y1="110.774" x2="18.5803" y2="110.774" stroke="white" strokeWidth="4">
                <animate attributeName='x2' from='18.5803' to='101.58' dur={speed} fill='freeze' begin='2.begin'/>
                <animate attributeName='y2' from='110.774' to='217.774' dur={speed} fill='freeze' begin='2.begin'/>
                <animate attributeName='x1' from='18.5803' to='101.58' dur={speed} fill='freeze' begin='3.begin'/>
                <animate attributeName='y1' from='110.774' to='217.774' dur={speed} fill='freeze' begin='3.begin'/>

                <animate attributeName='x2' from='101.58' to='18.5803' dur='.01s' fill='freeze' begin='3.end;'/>
                <animate attributeName='y2' from='217.774' to='110.774' dur='.01s' fill='freeze' begin='3.end;'/>
                <animate attributeName='x1' from='101.58' to='18.5803' dur='.01s' fill='freeze' begin='3.end;'/>
                <animate attributeName='y1' from='217.774' to='110.774' dur='.01s' fill='freeze' begin='3.end;'/>
            </line>
        </svg>
    )
}

export default Loading;