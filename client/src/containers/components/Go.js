import React, {useRef, useEffect} from 'react';

function Go(props) {

    const self = useRef(null)

    useEffect(() => {
        if (props.hide) {
            hide()
        }
    }, [props.hide])

    const hide = () => {
        self.current.onClick = () => {}
        self.current.animate(
            [
                {background: 'white'},
                {background: 'black'}
            ],
            {
                duration: 200,
                fill: 'forwards'
            }
        )
        self.current.childNodes.forEach(c => {
            c.animate(
                [
                    {background: 'black'},
                    {background: 'white'}
                ],
                {
                    duration: 200,
                    fill: 'forwards'
                }
            )
        })
        setTimeout(() => {
            self.current.childNodes.forEach((c, i) => {
                c.animate(
                    [
                        {transform: `translateX(10%)`, background: 'white'},
                        {transform: `translateX(${200 + (3-i)*40}%)`, background: 'black'}
                    ],
                    {
                        duration: 200,
                        fill: 'forwards'
                    }
                )
            })
            setTimeout(() => {
                self.current.style.display = 'none'
            }, 200)
        }, 100)
    }

    return (
        <div className='go' ref={self} onClick={props.onClick}>
            <div className='triangle'></div>
            <div className='triangle'></div>
            <div className='triangle'></div>
        </div>
    )
}

export default Go;