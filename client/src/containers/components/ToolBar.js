import { connect } from "react-redux"
import undoSrc from '../../stuff/undo.png'

function ToolBar(props) {

    const undo = () => {
        if (props.health > 0 && props.select.indexOf(-1) !== -1) {
            let selectTemp = props.select
            let target = props.select.indexOf(-1) - 1
            if (target !== 0) {
            selectTemp[target] = -1
            props.dispatch({type: 'SetSelect', value: selectTemp})
            props.dispatch({type: 'SetHealth', value: props.health - 1})
            }
        }
    }

    const healthScale = () => {
        let scale = []
        for (let i = 0; i < props.maxHealth; i++) {
            scale.push((
            <div className={`point ${props.health < i + 1 ? 'dead' : ''}`} key={i}></div>
            ))
        }
        return scale
    }

    return (
        <div className={`toolBar ${props.hidden ? 'hidden' : ''}`}>
          <img src={undoSrc} alt='undo' onClick={undo}/>
          <div className='health'>{healthScale()}</div>
        </div>
    )
}

const mapStateToProps = (state) => ({
    select: state.game.select,
    maxHealth: state.game.maxHealth,
    health: state.game.health,
  });
  export default connect(mapStateToProps)(ToolBar)