const initialState = {
    deepnes: 3,
    root: '',
    direction: true
}

export default function fullscreen(state = initialState, action) {
    switch (action.type) {
        case 'SetDeepnes':
            return (
                {
                    ...state,
                    deepnes: action.value
                }
            )
        case 'SetRoot':
            return (
                {
                    ...state,
                    root: action.value
                }
            )
        case 'SetDirection':
            return (
                {
                    ...state,
                    direction: action.value
                }
            )
        default:
            return state
    }
}
