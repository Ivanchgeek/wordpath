const initialState = {
    view: "settings"
}

export default function fullscreen(state = initialState, action) {
    switch (action.type) {
        case "SetView":
            return (
                {
                    ...state,
                    view: action.value
                }
            )
        default:
            return state
    }
}
