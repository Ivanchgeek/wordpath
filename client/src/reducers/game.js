const initialState = {
    tree: {},
    select: [],
    answer: [],
    answerResult: '',
    maxHealth: 0,
    health: 0
}

export default function fullscreen(state = initialState, action) {
    switch (action.type) {
        case 'SetTree':
            return (
                {
                    ...state,
                    tree: {...action.value}
                }
            )
        case 'SetSelect': 
            return (
                {
                    ...state,
                    select: [...action.value]
                }
            )
        case 'SetAnswer':
            return (
                {
                    ...state,
                    answer: [...action.value]
                }
            )
        case 'SetAnswerResult':
            return (
                {
                    ...state,
                    answerResult: action.value
                }
            )
        case 'SetMaxHealth':
            return (
                {
                    ...state,
                    maxHealth: action.value
                }
            )
        case 'SetHealth':
            return (
                {
                    ...state,
                    health: action.value
                }
            )
        default:
            return state
    }
}
