import { combineReducers } from 'redux'
import view from './view'
import settings from './settings'
import game from './game'

export default combineReducers({
    view,
    settings,
    game
})
