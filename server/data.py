import requests
from bs4 import BeautifulSoup
import random

payload = {'age1': '16', 'age2': '22', 'prof': '0', 'POL': 'MJ', 'normalize': '1', 'sort': '1'}

def response(word):
    formData = payload
    formData['word'] = word
    formData['type'] = 's'
    return process(formData)

def stimulus(word):
    formData = payload
    formData['word'] = word
    formData['type'] = 'r'
    return process(formData)

def process(formData):
    soup = BeautifulSoup(requests.post('http://tesaurus.ru/dict/', data=formData).text, 'html.parser')
    if ("Реакции на стимул в БД не обнаружены. Измените параметры поиска!" in soup.prettify()):
        alts = soup.find_all('b')
        if len(alts) > 1:
            formData['word'] = alts[1].contents[0]
            soup = BeautifulSoup(requests.post('http://tesaurus.ru/dict/', data=formData).text, 'html.parser')
        else:
            return['error', 'error']
    trs = soup.find_all('table')[1].find_all('tr')
    del trs[0]
    words = [tr.td.contents[0] for tr in trs]
    words = [word for word in words if (len(word) > 4) and (word.isalpha())]
    if (len(words) < 2):
        return['error', 'error']
    first = random.choice(words)
    words.remove(first)
    return [first, random.choice(words)]

