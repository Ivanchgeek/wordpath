from flask import Flask
from flask import request
import json

import data

app = Flask(__name__, static_url_path='')

@app.route('/r')
def response():
    return json.dumps(data.response(request.args.get('word')))

@app.route('/s')
def stimulus():
    return json.dumps(data.stimulus(request.args.get('word')))

@app.route('/')
def home():
    return app.send_static_file('index.html')

if __name__ == '__main__':
    from waitress import serve
    serve(app, host="0.0.0.0", port=80)