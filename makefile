build:
	cd ./client/ && yarn build
	sudo docker build -t wp:latest ./

run:
	sudo docker run -d -p 80:80 wp:latest

kill:
	sudo docker stop $$(sudo docker ps -a -q --filter ancestor=wp:latest --format="{{.ID}}")